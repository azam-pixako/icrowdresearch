$(function () {

    window.verifyRecaptchaCallback = function (response) {
        $('input[data-recaptcha]').val(response).trigger('change');
    }

    window.expiredRecaptchaCallback = function () {
        $('input[data-recaptcha]').val("").trigger('change');
    }

    $('#contact-form').validator();

    $('#contact-form').on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            var url = "contact.php";

            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                cache: false,
                beforeSend: function(){
                  $('#contact_submit').hide();
                  $('#loaderText').show();
                  $('#ajax_loader').show();
                },
                complete: function(){
                   $('#contact_submit').show();
                   $('#loaderText').hide();
                   $('#ajax_loader').hide();
                },
                success: function (data) {
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
                    if (messageAlert && messageText) {
                        $('#contact-form').find('.messages').html(alertBox);
                        $('#contact-form')[0].reset();
                        grecaptcha.reset();
                    }
                   $('html, body').animate({
                      scrollTop: $("#contact_form_section").offset().top
                   }, 2000);


                }
            });
            return false;
        }
    })
});