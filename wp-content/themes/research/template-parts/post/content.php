<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>
	<header class="entry-header">
		<?php


		if ( is_single() ) {
			the_title( '<h1 class="entry-title" style="display: inline-block; width: 70%">', '</h1>' );
		} elseif ( is_front_page() && is_home() ) {
			the_title( '<h3 class="entry-title" style="display: inline-block; width: 70%"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		} else {
			the_title( '<h2 class="entry-title" style="display: inline-block; width: 70%"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}
		?>
      <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
         <div class="post-thumbnail" style="float: left">
            <a href="<?php the_permalink(); ?>">
               <?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
            </a>
         </div><!-- .post-thumbnail -->
      <?php else: ?>
         <img style="float: left;" src="<?php bloginfo('template_directory'); ?>/assets/images/post_default_thumbnail.jpg" alt="<?php the_title(); ?>" />
      <?php endif; ?>
      <?php
      if ( 'post' === get_post_type() ) {
         echo '<div class="entry-meta">';
         if ( is_single() ) {
            twentyseventeen_posted_on();
         } else {
            echo twentyseventeen_time_link();
            twentyseventeen_edit_link();
         };
         echo '</div><!-- .entry-meta -->';
      } ?>
	</header><!-- .entry-header -->




	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );

		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
	if ( is_single() ) {
		twentyseventeen_entry_footer();
	}
	?>

</article><!-- #post-## -->
