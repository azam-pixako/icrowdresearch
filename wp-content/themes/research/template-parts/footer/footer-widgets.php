<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php
if ( is_active_sidebar( 'sidebar-2' ) ||
	 is_active_sidebar( 'sidebar-3' ) ) :
?>

	<aside class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Footer', 'twentyseventeen' ); ?>">
		<?php
		if ( is_active_sidebar( 'sidebar-2' ) ) { ?>

         <div class="widget-column footer-widget-1">
            <div class="footer-left">
               <div class="social_icons">
                   <?php dynamic_sidebar( 'sidebar-2' ); ?>
               </div>
            </div>
			</div>
		<?php }
		if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
			<div class="widget-column footer-widget-2">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div>
		<?php } ?>
      <div class="widget-column footer-widget-2" >

      <div class="resources" style="width: 29%;">
         <h3>Resources</h3>
         <?php wp_nav_menu( array('menu' => 'Footer-Resources-Menu' )); ?>
      </div>
         <div class="footerLogo" >
            <a style="text-decoration: none" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo1.png" alt="logo" /></a>
         </div>

      </div>


	</aside><!-- .widget-area -->

<?php endif; ?>
