<?php
/**
 * Load TGM Plugin
 */
if( !function_exists('gillion_register_required_plugins') && is_admin() ) :
    require_once ( trailingslashit( get_template_directory() ) . '/inc/plugins/TGM-Plugin-Activation/class-tgm-plugin-activation.php' );
    function gillion_register_required_plugins() {

        tgmpa(array(
            array(
                'name'      => esc_html__( 'Unyson', 'gillion' ),
                'slug'      => 'unyson',
                'required'  => true,
            ),

            array(
                'name'      => esc_html__( 'WPBakery Page Builder (formerly Visual Composer)', 'gillion' ),
                'slug'      => 'js_composer',
                'source'    => 'https://cdn.shufflehound.com/theme-plugins/visual-composer-OL6A44.zip',
                'required'  => true,
                'version'   => '5.4.5',
            ),

            array(
                'name'      => esc_html__( 'Revolution slider', 'jevelin' ),
                'slug'      => 'revslider',
                'source'    => 'https://cdn.shufflehound.com/theme-plugins/slider-revolution-QB4L22.zip',
                'required'  => false,
                'version'   => '5.4.6.4',
            ),

            array(
                'name'      => esc_html__( 'WooCommerce', 'jevelin' ),
                'slug'      => 'woocommerce',
                'required'  => false,
            ),

            array(
                'name'      => esc_html__( 'One Click Demo Install (optional)', 'gillion' ),
                'slug'      => 'one-click-demo-import',
                'required'  => false,
            ),

            array(
                'name'      => esc_html__( 'oAuth Twitter Feed for Developers (optional)', 'gillion' ),
                'slug'      => 'oauth-twitter-feed-for-developers',
                'required'  => false,
            ),

            array(
                'name'      => esc_html__( 'WP Instagram Widget (optional)', 'gillion' ),
                'slug'      => 'wp-instagram-widget',
                'required'  => false,
            ),

            array(
                'name'      => esc_html__( 'MailChimp for WordPress (optional)', 'gillion' ),
                'slug'      => 'mailchimp-for-wp',
                'required'  => false,
            ),

            array(
                'name'      => esc_html__( 'Gillion Extras (optional)', 'gillion' ),
                'slug'      => 'gillion-extras',
                'source'    => trailingslashit( get_template_directory() ) . '/inc/plugins/gillion-extras.tar',
                'required'  => false,
                'version'   => '1.0.0',
            ),

            array(
                'name'      => esc_html__( 'Envato Market Plugin (optional - for theme automatic updates)', 'jevelin' ),
                'slug'      => 'envato-wordpress-toolkit',
                'source'    => trailingslashit( get_template_directory() ) . '/inc/plugins/envato-market.zip',
                'required'  => false,
                'version'   => '1.0.0-RC2',
            ),

        ), array( 'is_automatic' => true ));

    }
    add_action( 'tgmpa_register', 'gillion_register_required_plugins' );
endif;
