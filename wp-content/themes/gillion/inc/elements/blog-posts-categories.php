<?php
/*
Element: Blog Posts Categories
*/

class vcBlogPostsCategories extends WPBakeryShortCode {

    function __construct() {
        add_action( 'init', array( $this, '_mapping' ) );
        add_shortcode( 'vcg_blog_posts_categories', array( $this, '_html' ) );
    }


    public function _mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) { return; }

        vc_map(
            array(
                'name' => __('Blog Posts Categories', 'gillion'),
                'base' => 'vcg_blog_posts_categories',
                'description' => __('Gillion posts organized by categories', 'gillion'),
                'category' => __('Gillion Elements', 'gillion'),
                //'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',
                'params' => array(

                    array(
                        'param_name' => 'style',
                        'heading' => __( 'Posts Style', 'gillion' ),
                        'description' => __( 'Choose slider style', 'gillion' ),
                        'value' => array(
                            __('Style 1 (grid layout categories posts)', 'gillion') => 'style1',
                            __('Style 2 (left slide with right posts scrollbar)', 'gillion') => 'style2',
                            __('Style 3 (small grid layout categories posts)', 'gillion') => 'style1 sh-categories-style3',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                        'admin_label' => true,
                    ),

                    array(
                        'param_name' => 'image_radious',
                        'heading' => __( 'Posts Image Radius', 'gillion' ),
                        'description' => __( 'Choose small posts image radius', 'gillion' ),
                        'value' => array(
                            __('8px (standard)', 'gillion') => '8px',
                            __('100%  (full circle)', 'gillion') => '100%',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                        "dependency" => array(
                            "element" => "style",
                            "value" => array( "style2" )
                        )
                    ),

                    array(
                        'param_name' => 'categories',
                        'heading' => __( 'Show Only Specific Categories', 'gillion' ),
                        'description' => __( 'Enter categories by names to narrow output (Note: only listed categories will be displayed, divide categories with linebreak (Enter)). By default will get most popular categories', 'gillion' ),
                        'value' => '',
                        'type' => 'exploded_textarea',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'limit',
                        'heading' => __( 'Posts Limit', 'gillion' ),
                        'description' => __( 'Choose posts limit', 'gillion' ),
                        'value' => '3',
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'order',
                        'heading' => __( 'Order', 'gillion' ),
                        'value' => array(
                            __('Ascending', 'gillion') => 'asc',
                            __('Descending', 'gillion') => 'desc',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'title',
                        'heading' => __( 'Title', 'gillion' ),
                        'description' => __( 'Enter categories title (works for style 2)', 'gillion' ),
                        'value' => 'Categories',
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => '',
                        "dependency" => array(
                            "element" => "style",
                            "value" => array( "style2" )
                        )
                    ),

                ),
            )
        );

    }


    public function _html( $atts ) {

        $atts['style'] = ( isset( $atts['style'] ) ) ? $atts['style'] : 'style1';
        $atts['limit'] = ( isset( $atts['limit'] ) ) ? $atts['limit'] : 3;
        $atts['order'] = ( isset( $atts['order'] ) ) ? $atts['order'] : 'asc';
        $atts['title'] = ( isset( $atts['title'] ) ) ? $atts['title'] : 'Categories';
        $atts['categories'] = ( isset( $atts['categories'] ) ) ? $atts['categories'] : implode( ",", array_slice( get_categories( array( 'fields' => 'id=>name', 'orderby' => 'count', 'order' => 'DESC', 'limit' => $atts['limit'] ) ) , 0, $atts['limit']) );
        $atts['image_radious'] = ( isset( $atts['image_radious'] ) ) ? $atts['image_radious'] : '8px';
        $rand = gillion_rand();
        ob_start();

            $id = 'blog-posts-categories-'.$rand;
            $css_class = array();
            $css_class[] = 'sh-categories';
            $css_class[] = $id;

            if( $atts['image_radious'] == '100%' ) :
                $css_class[] = 'sh-categories-round';
            endif;

            $categories = ( isset($atts['categories']) && $atts['categories'] ) ? str_replace("post:","", $atts['categories'] ) : '';
            $categories = explode( ",", $categories );
            if( isset( $atts['order'] ) && $atts['order'] == 'desc' ) :
                $categories = array_reverse( $categories );
            endif;
            ?>

            <?php if( $atts['style'] == 'style2' ) : ?>

                <div class="<?php echo esc_attr( implode( " ", $css_class ) ); ?> sh-categories-style2">
                    <?php if( count($categories) > 0 ) : ?>

                        <div class="sh-categories-tabs">
                            <div class="sh-categories-title">
                                <h2>
                                    <?php echo esc_attr( $atts['title'] ); ?>
                                </h2>
                            </div>
                            <div class="sh-categories-line" style="width: 99%;">
                                <div class="sh-categories-line-container"></div>
                            </div>
                            <div class="sh-categories-names">
                                <!-- Tabs -->
                                <ul class="nav nav-tab" role="tablist">
                                    <?php $i = 0;
                                    foreach( $categories as $category ) :
                                        $i++; $id = 'tab-'. esc_attr($rand) .'-'.$i;
                                    ?>
                                        <li role="presentation" class="<?php echo ($i == 1) ? ' active' : ''; ?>">
                                            <a href="#<?php echo esc_attr( $id ); ?>" role="tab" data-toggle="tab">
                                                <?php echo esc_attr( $category ); ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>

                        <!-- Content -->
                        <div class="tab-content">
                            <?php $i = 0;
                            foreach( $categories as $category ) :
                                $i++; $id = 'tab-'. esc_attr($rand) .'-'.$i;
                            ?>
                                <div role="tabpanel" class="tab-pane fade<?php echo ($i == 1) ? ' in active' : ''; ?>" id="<?php echo esc_attr( $id ); ?>">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <?php
                                                $cat_details = term_exists( $category, 'category' );
                                                if( isset( $cat_details['term_id'] ) && $cat_details['term_id'] > 0 ) :
                                                    $cat_query = 'cat';
                                                    $cat_id = $cat_details['term_id'];
                                                else :
                                                    $cat_query = 'category_name';
                                                    $cat_id = $category;
                                                endif;

                                                $limit = ( is_numeric($atts['limit']) ) ? intval( $atts['limit'] )+1 : 6;
                                                $posts = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => $limit, $cat_query => $cat_id ) );
                                                if( count($posts) > 0 ) : $j = 0;
                                                    while ( $posts->have_posts() ) : $posts->the_post(); $j++;
                                                ?>

                                                    <?php if ( $j == 1 ) :
                                                        set_query_var( 'style', 'cover-large' );
                                                        if( get_post_format() ) :
                                                            get_template_part( 'content', 'format-'.get_post_format() );
                                                        else :
                                                            get_template_part( 'content' );
                                                        endif;
                                                        ?>

                                                        </div>
                                                        <div class="col-md-5 sh-categories-list">
                                                    <?php else :

                                                        gillion_post_mini_layout( 'large' );

                                                    endif; ?>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    <?php endif; ?>
                </div>

            <?php else : ?>

                <div class="<?php echo esc_attr( implode( " ", $css_class ) ); ?> sh-categories-<?php echo esc_attr( $atts['style'] ); ?> row">
                    <?php
                    if( count($categories) > 0 ) :
                        $mini_layout = ( $atts['style'] == 'style1' ) ? 'layout1' : 'layout2';
                        set_query_var( 'style', 'cover-small' );
                        set_query_var( 'custom_thumb', 'post-thumbnail' );
                        foreach( $categories as $category ) : ?>

                            <div class="col-md-4 blog-style-cover">
                                <div class="sh-categories-tabs">
                                    <div class="sh-categories-title">
                                        <h2>
                                            <?php echo esc_attr( $category ); ?>
                                        </h2>
                                    </div>
                                    <div class="sh-categories-line" style="width: 99%;">
                                        <div class="sh-categories-line-container"></div>
                                    </div>
                                </div>

                                <?php
                                $cat_details = term_exists( $category, 'category' );
                                if( isset( $cat_details['term_id'] ) && $cat_details['term_id'] > 0 ) :
                                    $cat_query = 'cat';
                                    $cat_id = $cat_details['term_id'];
                                else :
                                    $cat_query = 'category_name';
                                    $cat_id = $category;
                                endif;

                                $limit = ( is_numeric($atts['limit']) ) ? intval( $atts['limit'] )+1 : 6;
                                $posts = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => $limit, $cat_query => $cat_id ) );
                                if( count($posts) > 0 ) : $i = 0;
                                    while ( $posts->have_posts() ) : $posts->the_post(); $i++;
                                        if( $i == 1 ) :

                                            if( get_post_format() ) :
                                                get_template_part( 'content', 'format-'.get_post_format() );
                                            else :
                                                get_template_part( 'content' );
                                            endif;

                                        else :

                                            gillion_post_mini_layout( '', $mini_layout );

                                        endif;
                                    endwhile;
                                    wp_reset_postdata();
                                endif; ?>

                            </div>

                        <?php endforeach;
                        set_query_var( 'style', '' );
                    endif;
                    ?>
                </div>

            <?php endif; wp_reset_postdata();


        //var_dump( $atts );
        return ob_get_clean();
    }

}
new vcBlogPostsCategories();
