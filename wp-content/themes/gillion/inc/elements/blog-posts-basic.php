<?php
/*
Element: Blog Posts
*/

class vcBlogPosts extends WPBakeryShortCode {

    function __construct() {
        add_action( 'init', array( $this, '_mapping' ) );
        add_shortcode( 'vcg_blog_posts', array( $this, '_html' ) );
    }


    public function _mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) { return; }

        vc_map(
            array(
                'name' => __('Blog Posts Basic', 'gillion'),
                'base' => 'vcg_blog_posts',
                'description' => __('Gillion basic blog posts', 'gillion'),
                'category' => __('Gillion Elements', 'gillion'),
                //'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',
                'params' => array(

                    array(
                        'param_name' => 'grid',
                        'heading' => __( 'Posts Style', 'gillion' ),
                        'description' => __( 'Choose posts style', 'gillion' ),
                        'value' => array(
                            __('Grid', 'gillion') => 'grid',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                        'admin_label' => true,
                    ),

                    array(
                        'param_name' => 'columns',
                        'heading' => __( 'Posts Columns', 'gillion' ),
                        'value' => array(
                            esc_html__('2 Columns', 'gillion') => '2',
                            esc_html__('3 Columns', 'gillion') => '3',
                            esc_html__('4 Columns', 'gillion') => '4',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'limit',
                        'heading' => __( 'Posts Limit', 'gillion' ),
                        'description' => __( 'Choose posts limit', 'gillion' ),
                        'value' => '2',
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'posts',
                        'heading' => __( 'Show Only Specific Posts', 'gillion' ),
                        'description' => __( 'Enter post IDs with comma, like: 1,2,3,4,5', 'gillion' ),
                        'value' => '',
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'offset',
                        'heading' => __( 'Posts Offset', 'gillion' ),
                        'description' => __( 'Enter posts offset number (will be disabled if specific posts will be entered)', 'gillion' ),
                        'value' => '',
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'categories',
                        'heading' => __( 'Show Only Specific Categories', 'gillion' ),
                        'description' => __( 'Enter categories by names to narrow output (Note: only listed categories will be displayed, divide categories with linebreak (Enter)).', 'gillion' ),
                        'value' => '',
                        'type' => 'exploded_textarea',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'lines',
                        'heading' => __( 'Hide Post Seperators', 'gillion' ),
                        'description' => __( 'Choose to enable or disable post seperator lines', 'gillion' ),
                        'value' => 'Yes',
                        'type' => 'checkbox',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'order_by',
                        'heading' => __( 'Order By', 'gillion' ),
                        'value' => array(
                            esc_html__('Date', 'gillion') => 'date',
                            esc_html__('Name', 'gillion') => 'name',
                            esc_html__('Author', 'gillion') => 'author',
                            esc_html__('Random', 'gillion') => 'rand',
                            esc_html__('Comment Count', 'gillion') => 'comment_count',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                    ),

                    array(
                        'param_name' => 'order2',
                        'heading' => __( 'Order', 'gillion' ),
                        'value' => array(
                            __('Descending', 'gillion') => 'DESC',
                            __('Ascending', 'gillion') => 'ASC',
                        ),
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => '',
                        'std' => 'desc',
                    ),

                    array(
                        'param_name' => 'title',
                        'heading' => __( 'Title', 'gillion' ),
                        'description' => __( 'Enter blog posts title', 'gillion' ),
                        'value' => 'Blog Posts',
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => '',
                    ),

                ),
            )
        );

    }


    public function _html( $atts ) {

        $atts['posts'] = ( isset( $atts['posts'] ) ) ? $atts['posts'] : '';
        $atts['style'] = ( isset( $atts['style'] ) ) ? $atts['style'] : 'style1';
        $atts['columns'] = ( isset( $atts['columns'] ) ) ? $atts['columns'] : '2';
        $atts['limit'] = ( isset( $atts['limit'] ) ) ? $atts['limit'] : 2;
        $atts['dots'] = ( isset( $atts['dots'] ) ) ? $atts['dots'] : true;
        $atts['categories'] = ( isset( $atts['categories'] ) ) ? $atts['categories'] : '';
        $atts['lines'] = ( isset( $atts['lines'] ) ) ? $atts['lines'] : false;
        $atts['title'] = ( isset( $atts['title'] ) ) ? $atts['title'] : 'Blog Posts';
        $offset = ( isset( $atts['offset'] ) ) ? intval( $atts['offset'] ) : 0;
        $order  = ( isset( $atts['order2'] ) ) ? $atts['order2'] : 'desc';
        $order_by = ( isset( $atts['order_by'] ) ) ? $atts['order_by'] : 'date';
        $rand = gillion_rand();
        ob_start();

            if( $atts['posts'] ) :
                $specific_posts = explode(',', $atts['posts']); $i=0;
                foreach( $specific_posts as $specific_post ) {
                    $specific_posts[$i] = intval( $specific_post );
                    $i++;
                }
            else :
                $specific_posts = array();
            endif;

            $id = 'blog-posts-'.esc_attr( $rand );
            $class = array();
            $class[] = 'sh-blog-standard-posts';
            $class[] = $id;

            $class2 = array();
            $class2[] = 'sh-group';
            $class2[] = 'blog-list';
            $class2[] = 'blog-style-grid';
            $class2[] = 'blog-style-grid-element';
            $class2[] = ( $atts['columns'] > 0 ) ? 'blog-style-columns'.$atts['columns'] : '';
            $class2[] = ( $atts['lines'] == true ) ? 'blog-hide-lines' : '';

            $categories_query = ( $atts['categories'] ) ? explode( ",", $atts['categories'] ) : '';
            if( count( $specific_posts ) > 0 ) :
                $posts = new WP_Query( array(
                    'post_type' => 'post',
                    'post__in' => $specific_posts,
                    'posts_per_page' => $atts['limit'],
                    'order' => $order,
                    'orderby' => $order_by
                ));
            else :
                $posts = new WP_Query( array(
                    'post_type' => 'post',
                    'category_name' => $atts['categories'],
                    'posts_per_page' => $atts['limit'],
                    'offset' => $offset,
                    'order' => $order,
                    'orderby' => $order_by
                ));
            endif;
            ?>
            <?php if( isset($atts['title']) && $atts['title'] ) : ?>
                <h2 class="sh-blog-fancy-title">
                    <?php echo esc_attr( $atts['title'] ); ?>
                </h2>
            <?php endif; ?>
            <div class="<?php echo esc_attr( implode( " ", $class ) ); ?>">
                <div class="<?php echo esc_attr( implode( " ", $class2 ) ); ?>">
                    <?php
                        if( count($posts) > 0 ) :
                            set_query_var( 'style', 'grid' );
                            while ( $posts->have_posts() ) : $posts->the_post();

                                if( get_post_format() ) :
                                    get_template_part( 'content', 'format-'.get_post_format() );
                                else :
                                    get_template_part( 'content' );
                                endif;

                            endwhile;
                        else :

                            get_template_part( 'content', 'none' );

                        endif;
                    ?>
                </div>
            </div>


        <?php  wp_reset_postdata();
        return ob_get_clean();
    }

}
new vcBlogPosts();
