<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$documentation_link = '//support.shufflehound.com/documentation/gillion/?source=theme&version=1';
$options = array(
	'documentation' => array(
		'title'   => esc_html__( 'Theme Documentation', 'gillion' ),
		'type'    => 'tab',
		'options' => array(
			'info-box' => array(
				'title'   => esc_html__( 'Theme Documentation', 'gillion' ),
				'type'    => 'box',
				'options' => array(

					'theme_documentation' => array(
					    'type'  => 'html-full',
					    'value' => false,
					    'label' => false,
					    'html'  => '
							<iframe class="gillion-live-documentation" src="'.esc_url( $documentation_link ).'" frameborder="0"></iframe>',
					)

				)
			),
		)
	)
);
