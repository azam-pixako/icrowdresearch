<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$support_link = '//support.shufflehound.com';
$options = array(
	'support' => array(
		'title'   => esc_html__( 'Theme Support', 'gillion' ),
		'type'    => 'tab',
		'options' => array(
			'info-box' => array(
				'title'   => esc_html__( 'Theme Support', 'gillion' ),
				'type'    => 'box',
				'options' => array(

					'theme_support' => array(
					    'type'  => 'html-full',
					    'value' => false,
					    'label' => false,
					    'html'  => '
							<iframe class="gillion-live-support" src="'.esc_url( $support_link ).'" frameborder="0"></iframe>',
					)

				)
			),
		)
	)
);
