<div class="blog-slider blog-slider-style3 <?php echo esc_attr( $id ); ?>" style="position: relative;">
    <div class="blog-slider-list slick-initialized">

        <div class="blog-slider-item" style="background-image: url( <?php echo esc_url( the_post_thumbnail_url( 'full' ) ); ?> );">
            <div class="blog-slider-container">
                <div class="blog-slider-content">

                    <div class="blog-slider-content-icon">
                        <i class="<?php
                            $format = get_post_format();
                            if( $format == 'gallery' ) :
                                echo 'icon icon-grid';
                            elseif( $format == 'video' ) :
                                echo 'icon icon-control-play';
                            elseif( $format == 'audio' ) :
                                echo 'icon icon-volume-2';
                            elseif( $format == 'link' ) :
                                echo 'icon icon-link';
                            elseif( $format == 'quote' ) :
                                echo 'ti-quote-left';
                            else :
                                echo 'icon icon-pencil';
                            endif;
                        ?>"></i>
                    </div>
                    <div class="blog-slider-content-details">
                        <?php gillion_post_categories(); ?>
                        <h1 class="post-title">
                            <?php the_title(); ?><?php gillion_post_readlater( get_the_ID() ); ?>
                        </h1>
                        <div class="post-meta">
                            <?php gillion_post_meta( 10 ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="blog-slider-dots"></div>
</div>
