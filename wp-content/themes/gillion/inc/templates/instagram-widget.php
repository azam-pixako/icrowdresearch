<?php $thumb_size = ( did_action( 'get_footer' ) ) ? 'small' : $size; ?>
<li class="<?php echo esc_attr( $liclass ); ?>">
    <a href="<?php echo esc_url( $item['link'] ); ?>" target="<?php echo esc_attr( $target ); ?>" class="<?php echo esc_attr( $aclass ); ?>">
        <img src="<?php echo esc_url( $item[$thumb_size] ); ?>"
             class="<?php echo esc_attr( $imgclass ); ?>"
             alt="<?php echo esc_html( $item['description'] ); ?>"
             title="<?php echo esc_html( $item['description'] ); ?>" />
        <div class="sh-instagram-meta">
            <div class="sh-instagram-meta-content">
                <span class="sh-instagram-likes">
                    <i class="icon icon-heart"></i> <?php echo esc_attr( $item['likes'] ); ?>
                </span>
                <span class="sh-instagram-comments">
                    <i class="icon icon-bubbles"></i> <?php echo esc_attr( $item['comments'] ); ?>
                </span>
            </div>
        </div>
    </a>
</li>
