<?php
ob_start("gillion_compress");
if( defined('FW') ) :

/*-----------------------------------------------------------------------------------*/
/* Define Variables
/*-----------------------------------------------------------------------------------*/

$body = gillion_font_option('styling_body');
$body_color = gillion_option_value('styling_body','color');
$body_line_height = gillion_option_value('styling_body','line-height');
$body_font = gillion_option_value('styling_body','family');
$body_background = gillion_option('styling_body_background');
$meta_color = gillion_option('styling_meta_color');

$meta_category_color = gillion_option('styling_meta_categories_color');
$meta_category_hover_color = gillion_option('styling_meta_categories_hover_color');
$meta_category_color_in_slider = gillion_option('styling_meta_categories_slider_color');
$meta_category_hover_color_in_slider = gillion_option('styling_meta_categories_slider_hover_color');
$meta_category_font = gillion_option_value('categories_font','family');
$meta_category_font_weight = gillion_option_value('categories_font','variation');

$accent_color =  gillion_option('accent_color');
$accent_hover_color = gillion_option('accent_hover_color');
$link_color = gillion_option('link_color');
$link_hover_color = gillion_option('link_hover_color');

$headings = gillion_font_option('styling_headings');
$heading_color = gillion_option_value('styling_headings','color');
$heading_font = gillion_option_value('styling_headings','family');
$heading1 = gillion_option('styling_h1');
$heading2 = gillion_option('styling_h2');
$heading3 = gillion_option('styling_h3');
$heading4 = gillion_option('styling_h4');
$heading5 = gillion_option('styling_h5');
$heading6 = gillion_option('styling_h6');
$headings_line = gillion_option( 'styling_headings_line', 'on' );
$widget_font_weight = gillion_option('styling_widget_font_weight');
$accent_element_font = gillion_option('accent_element_font');
$additional_font = gillion_option_value('additional_font','family');
$post_title_font = gillion_option('post_title_font');
$post_title_uppercase = gillion_option('post_title_uppercase', false);
$single_content_size = gillion_option('styling_single_content_size', '15');

$header_width = gillion_option('header_width');
$header_uppercase = gillion_option('header_uppercase');
$header_mobile_uppercase = gillion_option('header_mobile_uppercase');
$header_background_color = gillion_option('header_background_color');
$header_background_image = gillion_option_image('header_background_image');
$header_text_color = gillion_option('header_text_color');
$header_border_color = gillion_option('header_border_color');
$topbar_background_color = gillion_option('header_top_background_color');
$topbar_background_image = gillion_option_image('header_top_background_image');
$topbar_color = gillion_option('header_top_color');
$topbar_hover_color = gillion_option('header_top_hover_color');

$header_nav_size = gillion_option('header_nav_size');
$header_mobile_nav_size = gillion_option('header_mobile_nav_size');
$header_nav_color = gillion_option('header_nav_color');
$header_nav_hover_color = gillion_option('header_nav_hover_color');
$header_nav_active_color = gillion_option('header_nav_active_color');
$header_nav_active_background_color = gillion_option('header_nav_active_background_color');
$header_height = ( intval( gillion_logo_height() ) + 30 );
if( $header_height < 70 ) :
	$header_height = 70;
endif;
$header_nav_icon_color = gillion_option('header_nav_icon_color');
$header_nav_icon_hover_color = gillion_option('header_nav_icon_hover_color');

$menu_background_color = gillion_option('menu_background_color');
$menu_link_border_color = gillion_option('menu_link_border_color');
$menu_link_color = gillion_option('menu_link_color');
$menu_link_hover_color = gillion_option('menu_link_hover_color');
$menu_link_border_color = gillion_option('menu_link_border_color');

$sidebar_headings = gillion_font_option('sidebar_headings');
$sidebar_border_color = gillion_option('sidebar_border_color');

$footer_width = gillion_option('footer_width');
$footer_background_image = gillion_option_image('footer_background_image');
$footer_background_color = gillion_option('footer_background_color');
$footer_text_color = gillion_option('footer_text_color');
$footer_icon_color = gillion_option('footer_icon_color');
$footer_headings = gillion_font_option('footer_headings');
$footer_border_color = gillion_option('footer_border_color');
$footer_bottom_border_color = gillion_option('footer_bottom_border_color');
$footer_link_color = gillion_option('footer_link_color');
$footer_hover_color = gillion_option('footer_hover_color');
$footer_columns =  gillion_option('footer_columns');
$footer_padding =  gillion_option('footer_padding');

$copyright_background_color = gillion_option('copyright_background_color');
$copyright_text_color = gillion_option('copyright_text_color');
$copyright_link_color = gillion_option('copyright_link_color');
$copyright_hover_color = gillion_option('copyright_hover_color');
$copyright_border_color = gillion_option('copyright_border_color');

$post_meta = gillion_option( 'post_meta' );
$wc_columns = gillion_option( 'wc_columns' );

$white_borders = gillion_option('white_borders', false);
$header_layout = gillion_option('header_layout', 1);
$page_layout_val = gillion_option('page_layout');
$page_layout = ( isset( $page_layout_val['page_layout'] ) ) ? esc_attr($page_layout_val['page_layout']) : 'line';
$page_layout_atts = gillion_get_picker( $page_layout_val );
$crispy_images = gillion_option('crispy_images', false);
$back_to_top_rounded = gillion_option('back_to_top_radius', true);
$rtl_support = gillion_option('rtl_support', false);

$page_404_background = ( gillion_option('404_background') ) ? gillion_option('404_background') : $accent_color;
$page_404_background2 = gillion_option('404_background2');

$titlebar_background_main = gillion_get_image( gillion_option( 'titlebar_background' ) );
$titlebar_background_page = gillion_get_image( gillion_post_option( gillion_page_id(), 'titlebar_background' ) );
$titlebar_background = ( $titlebar_background_page ) ? $titlebar_background_page : $titlebar_background_main;

$header_bottom_border = gillion_post_option( gillion_page_id(), 'header_bottom_border' );

$page = (get_query_var('page')) ? get_query_var('page') : 1;
$page = (get_query_var('paged')) ? get_query_var('paged') : $page;
?>

/* Gillion CSS */

<?php
/*-----------------------------------------------------------------------------------*/
/* Body
/*-----------------------------------------------------------------------------------*/
?>

	.sh-default-color a,
	.sh-default-color,
	#sidebar a:not(.widget-learn-more),
	.logged-in-as a ,
	.wpcf7-form-control-wrap .simpleselect {
		color: <?php echo esc_attr( $body_color ); ?>!important;
	}

	html body,
	html .menu-item a {
		<?php echo wp_kses_post( $body ); ?>
		<?php if( $body_background != '#ffffff' ) : ?>
			background-color: <?php echo esc_attr( $body_background ); ?>;
		<?php endif; ?>
	}

	<?php if( $body_line_height > 0 ) : ?>
		body p {
			line-height: <?php echo esc_attr( $body_line_height ); ?>px;
		}
	<?php endif; ?>

	<?php if( $meta_color ) : ?>
		.post-thumnail-caption,
		.post-meta a,
		.post-meta span,
		.sh-pagination a,
		.sh-pagination span,
		#sidebar .widget_recent_entries .post-date {
			color: <?php echo esc_attr( $meta_color ); ?>;
		}

		#sidebar .widget_recent_comments .recentcomments > span a,
		#sidebar .post-meta a {
			color: <?php echo esc_attr( $meta_color ); ?>!important;
		}
	<?php endif; ?>

	<?php if( $meta_category_color && $meta_category_color != '#d79c6a' ) : ?>
		.post-categories,
		.post-categories a {
			color: <?php echo esc_attr( $meta_category_color ); ?>;
		}
	<?php endif; ?>

	<?php if( $meta_category_hover_color ) : ?>
		.post-categories a:hover,
		.post-categories a:focus {
			color: <?php echo esc_attr( $meta_category_hover_color ); ?>;
		}
	<?php endif; ?>


	<?php if( $meta_category_color_in_slider ) : ?>
		.blog-slider-item .post-categories,
		.blog-slider-item .post-categories a,
		.post-style-cover .post-categories,
		.post-style-cover .post-categories a {
			color: <?php echo esc_attr( $meta_category_color_in_slider ); ?>;
		}
	<?php endif; ?>

	<?php if( $meta_category_hover_color_in_slider ) : ?>
		.blog-slider-item a:hover,
		.blog-slider-item a:focus,
		.post-style-cover a:hover,
		.post-style-cover a:focus {
			color: <?php echo esc_attr( $meta_category_hover_color_in_slider ); ?>;
		}
	<?php endif; ?>


	<?php if( $meta_category_font ) :
		$meta_category_font_weight = ( $meta_category_font_weight == 'regular' ) ? '400' : $meta_category_font_weight;
	?>
		.cat-item a,
		.post-categories,
		.post-categories a {
			font-family: <?php echo esc_attr( $meta_category_font ); ?>;
			font-weight: <?php echo esc_attr( $meta_category_font_weight ); ?>
		}
	<?php endif; ?>

<?php
/*-----------------------------------------------------------------------------------*/
/* Accent colors
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( class_exists( 'woocommerce' ) ) : ?>
	.woocommerce-header.step1 .woocommerce-header-item-cart .woocommerce-header-icon i,
	.woocommerce-header.step2 .woocommerce-header-item-checkout .woocommerce-header-icon i,
	.woocommerce-header.step3 .woocommerce-header-item-complate .woocommerce-header-icon i,
	<?php endif; ?>
	.sh-accent-color,
	ul.page-numbers a:hover,
	.sh-comment-date a:hover,
	.comment-respond #cancel-comment-reply-link,
	.post-sticky {
		color: <?php echo esc_attr( $accent_color ); ?>!important;
	}

	<?php if( class_exists( 'woocommerce' ) ) : ?>
		.woocommerce-header.step1 .woocommerce-header-content .woocommerce-header-item-cart .woocommerce-header-icon,
		.woocommerce-header.step2 .woocommerce-header-content .woocommerce-header-item-checkout .woocommerce-header-icon,
		.woocommerce-header.step3 .woocommerce-header-content .woocommerce-header-item-complate .woocommerce-header-icon {
		    border-color: <?php echo esc_attr( $accent_color ); ?>;
		}

		.gillion-woocommerce nav.woocommerce-MyAccount-navigation ul li.woocommerce-MyAccount-navigation-link.is-active {
		    border-left-color: <?php echo esc_attr( $accent_color ); ?>!important;
		}
	<?php endif; ?>

	.sh-dropcaps-full-square,
	.sh-dropcaps-full-square-border,
	.mc4wp-form input[type=submit],
	.mc4wp-form button[type=submit],
	.gillion-woocommerce .woocommerce .return-to-shop a.button {
		background-color: <?php echo esc_attr( $accent_color ); ?>;
	}

	<?php if( class_exists( 'woocommerce' ) ) : ?>
	.woocommerce-header.step1 .woocommerce-header-item-cart .woocommerce-header-icon span,
	.woocommerce-header.step1 .woocommerce-header-item-cart:after,
	.woocommerce-header.step2 .woocommerce-header-item-checkout .woocommerce-header-icon span,
	.woocommerce-header.step2 .woocommerce-header-item-checkout:before,
	.woocommerce-header.step2 .woocommerce-header-item-checkout:after,
	.woocommerce-header.step3 .woocommerce-header-item-complate .woocommerce-header-icon span,
	.woocommerce-header.step3 .woocommerce-header-item-complate:before,
	.woocommerce #payment #place_order, .woocommerce-page #payment #place_order,
	.gillion-woocommerce div.product form.cart .button:hover,
	.gillion-woocommerce .sh-nav .widget_shopping_cart .buttons a.checkout,
	.gillion-woocommerce a.button.alt:hover,
	<?php endif; ?>
	.contact-form input[type="submit"],
	.sh-back-to-top:hover,
	.sh-dropcaps-full-square-tale,
	ul.page-numbers .current,
	ul.page-numbers .current:hover,
	.comment-input-required,
	.widget_tag_cloud a:hover,
	.post-password-form input[type="submit"],
	.wpcf7-form .wpcf7-submit {
		background-color: <?php echo esc_attr( $accent_color ); ?>!important;
	}

	::selection {
		background-color: <?php echo esc_attr( $accent_color ); ?>!important;
		color: #fff;
	}
	::-moz-selection {
		background-color: <?php echo esc_attr( $accent_color ); ?>!important;
		color: #fff;
	}

	.sh-dropcaps-full-square-tale:after,
	.widget_tag_cloud a:hover:after {
		border-left-color: <?php echo esc_attr( $accent_color ); ?>!important;
	}

	.sh-back-to-top:hover {
		border-color: <?php echo esc_attr( $accent_color ); ?>!important;
	}

	<?php if($accent_hover_color) : ?>
		.contact-form input[type="submit"]:hover,
		.wpcf7-form .wpcf7-submit:hover,
		.post-password-form input[type="submit"]:hover,
		.mc4wp-form input[type=submit]:hover {
			background-color: <?php echo esc_attr( $accent_hover_color ); ?>!important;
		}
	<?php endif; ?>

<?php
/*-----------------------------------------------------------------------------------*/
/* Links
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $link_color ) : ?>
		a {
			color: <?php echo esc_attr( $link_color ); ?>;
		}
	<?php endif; ?>

	<?php if( $link_hover_color ) : ?>
		a:hover,
		a:focus {
			color: <?php echo esc_attr( $link_hover_color ); ?>;
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Headings
/*-----------------------------------------------------------------------------------*/
?>

	body h1,
	body h2,
	body h3,
	body h4,
	body h5,
	body h6 {
		<?php echo wp_kses_post( $headings ); ?>
	}

	.post-meta,
	.post-categories,
	.post-switch-item-right,
	.sh-read-later-review-score,
	.sh-nav li.menu-item a,
	.sh-nav-container li.menu-item a,
	.sh-comment-date a,
	.post-button .post-button-text,
	.widget_categories li,
	.sh-dropcaps,
	.sh-dropcaps-full-square,
	.sh-dropcaps-full-square-border,
	.sh-dropcaps-full-square-tale,
	.sh-dropcaps-square-border,
	.sh-dropcaps-square-border2,
	.sh-dropcaps-circle,
	.comment-body .reply,
	.sh-comment-form label,
	blockquote,
	blockquote:after,
	.post-review-score,
	.sh-comment-author a,
	.sh-header-top .sh-nav li.menu-item a,
	.post-quote-link-content p,
	.instagram-post-overlay-container,
	.widget_categories li .count,
	.sh-login-popup,
	.widget-learn-more,
	.gillion-woocommerce ul.products li.product,
	.gillion-woocommerce div.product div.summary > *:not(.woocommerce-product-details__short-description),
	.gillion-woocommerce div.product .woocommerce-tabs ul.tabs li a,
	.gillion-woocommerce #review_form,
	.gillion-woocommerce .widget_shopping_cart .cart_list > li > a:not(.remove),
	.gillion-woocommerce .widget_shopping_cart .total,
	.gillion-woocommerce .woocommerce-MyAccount-navigation ul li,
	.gillion-woocommerce table thead,
	body.woocommerce-account.woocommerce-page:not(.woocommerce-edit-address) .woocommerce-MyAccount-content > p {
		font-family: "<?php
		if( $accent_element_font == 'body') :
			echo esc_attr( $body_font );
		elseif( $accent_element_font == 'meta') :
			echo esc_attr( $meta_category_font );
		else :
			echo esc_attr( $heading_font );
		endif;
		?>";
	}

	<?php if( $post_title_font != 'heading') : ?>
		.post-title,
		.post-title > h1,
		.post-title > h2,
		.post-title > h3,
		.post-title > h5 {
			font-family: "<?php
			if( $post_title_font == 'body') :
				echo esc_attr( $body_font );
			elseif( $post_title_font == 'meta') :
				echo esc_attr( $meta_category_font );
			elseif( $post_title_font == 'additional') :
				echo esc_attr( $additional_font );
			endif;
			?>";
		}
	<?php endif; ?>

	.sh-heading-font {
		font-family: "<?php echo esc_attr( $heading_font ); ?>"
	}

	<?php if( $heading1 ) : ?>
		h1 {
			font-size: <?php echo esc_attr( $heading1 ); ?>px;
		}
	<?php endif; ?>

	<?php if( $heading2 ) : ?>
		h2 {
			font-size: <?php echo esc_attr( $heading2 ); ?>px;
		}
	<?php endif; ?>

	<?php if( $heading3 ) : ?>
		h3 {
			font-size: <?php echo esc_attr( $heading3 ); ?>px;
		}
	<?php endif; ?>

	<?php if( $heading4 ) : ?>
		h4 {
			font-size: <?php echo esc_attr( $heading4 ); ?>px;
		}
	<?php endif; ?>

	<?php if( $heading5 ) : ?>
		h5 {
			font-size: <?php echo esc_attr( $heading5 ); ?>px;
		}
	<?php endif; ?>

	<?php if( $heading6 ) : ?>
		h6 {
			font-size: <?php echo esc_attr( $heading6 ); ?>px;
		}
	<?php endif; ?>

	<?php if( $headings_line == 'off' ) : ?>
		.sh-footer .post-meta-content>*:not(:last-child):not(:nth-last-child(2)):after,
		.sh-footer-widgets h3:not(.widget-tab-title):after,
		.sh-footer-widgets .sh-widget-poststab-title:after,
		#sidebar h3:after,
		.post-single-title:after,
		.sh-widget-poststab-title:after,
		.sh-blog-fancy-title:after {
			height: 0px;
		}
	<?php endif; ?>

	<?php if( $post_title_uppercase ) : ?>
		.post-title {
			text-transform: uppercase;
		}
	<?php endif; ?>

	<?php if( $single_content_size && $single_content_size != '15' ) : ?>
		.blog-single .post-content/*
		.sh-text-content .page-content*/ {
			font-size: <?php echo esc_attr( $single_content_size ); ?>px;
		}
	<?php endif; ?>

	<?php if( $widget_font_weight && $widget_font_weight != 'default' ) : ?>
		.widget-title,
		.post-single-title,
		.sh-post-author .sh-post-author-info h1,
		.sh-post-author .sh-post-author-info h4,
		.comment-reply-title {
			font-weight: <?php echo esc_attr( $widget_font_weight ); ?>;
		}
	<?php endif; ?>

	table th,
	.blog-single .post-title h2:hover,
	.wrap-forms label,
	.wpcf7-form p,
	.post-password-form label,
	#sidebar .widget_categories li > a,
	#sidebar .widget_categories li .count,
	#sidebar .sh-widget-posts-slider-group-style2 .post-categories a,
	#sidebar .sh-widget-posts-slider-group-style3 .post-categories a,
	.sh-footer-widgets .sh-widget-posts-slider-group-style2 .post-categories a,
	.sh-footer-widgets .sh-widget-posts-slider-group-style3 .post-categories a,
	.sh-comment-author,
	.post-meta a:hover,
	.post-meta a:focus,
	.sh-comment-author a,
	.blog-textslider-post a,
	.gillion-woocommerce .price > ins,
	.gillion-woocommerce ul.products li.product .price > span.amount,
	.gillion-woocommerce p.price,
	.gillion-woocommerce ul.products li.product .woocommerce-loop-product__title,
	.gillion-woocommerce ul.products li.product .outofstock,
	.gillion-woocommerce .widget_shopping_cart .cart_list > li > a:not(.remove),
	.gillion-woocommerce .widget_shopping_cart .total,
	.gillion-woocommerce .widget_shopping_cart .buttons a,
	.gillion-woocommerce .widget_shopping_cart .buttons a:not(.checkout) {
		color: <?php echo esc_attr( $heading_color ); ?>!important;
	}


<?php
/*-----------------------------------------------------------------------------------*/
/* Header
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $header_background_color ) : ?>
		.sh-header,
		.sh-header-top,
		.sh-header-mobile {
			background-color: <?php echo esc_attr( $header_background_color ); ?>;
		}
	<?php endif; ?>

	<?php if( $topbar_background_color ) : ?>
		.sh-header-top {
			background-color: <?php echo esc_attr( $topbar_background_color ); ?>!important;
		}
	<?php endif; ?>

	<?php if( $topbar_color && $topbar_color != '#ffffff' ) : ?>
		.sh-header-top .sh-nav li.menu-item a,
		.sh-header-top .header-social-media a,
		.sh-header-top-date {
			color: <?php echo esc_attr( $topbar_color ); ?>;
		}
	<?php endif; ?>

	<?php if( $topbar_hover_color && $topbar_hover_color != '#b1b1b1' ) : ?>
		.sh-header-top .sh-nav li.menu-item a:hover,
		.sh-header-top .header-social-media a:hover,
		.sh-header-top-date:hover {
			color: <?php echo esc_attr( $topbar_hover_color ); ?>;
		}
	<?php endif; ?>



	<?php if( $header_background_image ) : ?>
		.sh-header,
		.sh-header-mobile-navigation {
			background-image: url(<?php echo esc_url( $header_background_image ); ?>);
			background-size: cover;
			background-position: 50% 50%;
		}
	<?php endif; ?>
	<?php if( $topbar_background_image ) : ?>
		.sh-header-top {
			background-image: url(<?php echo esc_url( $topbar_background_image ); ?>);
		}
	<?php endif; ?>

	<?php if( $header_uppercase == true ) : ?>
		.sh-header .sh-nav > li.menu-item > a {
			text-transform: uppercase;
		}
	<?php endif; ?>

	<?php if( $header_mobile_uppercase == true ) : ?>
		.sh-nav-mobile li a {
			text-transform: uppercase;
		}
	<?php endif; ?>

	<?php if( $header_nav_color ) : ?>
		.sh-header-search-close i,
		.sh-header .sh-nav > li.menu-item > a,
		.sh-header-mobile-navigation li.menu-item > a > i {
			color: <?php echo esc_attr( $header_nav_color ); ?>;
		}

		.sh-header .sh-nav-login #header-login > span {
			border-color: <?php echo esc_attr( $header_nav_color ); ?>;
		}
	<?php endif; ?>

	.sh-header .sh-nav > li > a i {
		color: <?php echo esc_attr( $header_nav_icon_color ); ?>;
	}

	.sh-header .sh-nav > li > a:hover i {
		color: <?php echo esc_attr( $header_nav_icon_hover_color ); ?>;
	}

	<?php if( $header_nav_size ) : ?>
		.sh-nav li.menu-item a {
			font-size: <?php echo esc_attr( gillion_addpx($header_nav_size) ); ?>;
		}
	<?php endif; ?>

	<?php if( $header_mobile_nav_size ) : ?>
		.sh-nav-mobile li a {
			font-size: <?php echo esc_attr( gillion_addpx($header_mobile_nav_size) ); ?>;
		}
	<?php endif; ?>

	<?php if( $header_nav_hover_color ) : ?>
		.sh-header .sh-nav > li.menu-item:hover:not(.sh-nav-social) > a,
		.sh-header .sh-nav > li.menu-item:hover:not(.sh-nav-social) > a > i,
		.sh-header .sh-nav > li.sh-nav-social > a:hover > i,
		.sh-header-mobile-navigation li > a:hover > i {
			color: <?php echo esc_attr( $header_nav_hover_color ); ?>;
		}

		.sh-header .sh-nav > li.menu-item:hover .sh-hamburger-menu span {
			background-color: <?php echo esc_attr( $header_nav_hover_color ); ?>;
		}
	<?php endif; ?>

	<?php if( $header_nav_active_color ) : ?>
		.sh-header .sh-nav > .current_page_item > a,
		.sh-header .sh-nav > .current-menu-ancestor > a {
			color: <?php echo esc_attr( $header_nav_active_color ); ?>!important;
		}
	<?php endif; ?>

	<?php if( $header_nav_active_background_color ) : ?>
		.sh-header .sh-nav > .current_page_item {
			background-color: <?php echo esc_attr( $header_nav_active_background_color ); ?>!important;
		}
	<?php endif; ?>

	<?php if( $header_height ) : ?>
		.header-logo img {
			height: <?php echo esc_attr( gillion_logo_height() ); ?>;
			max-height: 250px;
		}

		.sh-header-mobile-navigation .header-logo img {
			height: <?php echo esc_attr( gillion_logo_height( 'responsive' ) ); ?>;
			max-height: 250px;
		}

		.sh-sticky-header-active .header-logo img {
			height: <?php echo esc_attr( gillion_logo_height( 'sticky' ) ); ?>;
		}
	<?php endif; ?>

	<?php if( $header_border_color ) : ?>
		.sh-header,
		.sh-header-3 > .container {
			border-bottom: 1px solid <?php echo esc_attr( $header_border_color ); ?>;
		}
	<?php endif; ?>

	<?php if( $header_width == 'full' ) : ?>
		.sh-header .container,
		.sh-header-top .container {
			width: 90%!important;
			max-width: 90%!important;
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Menu
/*-----------------------------------------------------------------------------------*/
?>

 	<?php if( $menu_background_color ) : ?>
		.sh-header-mobile-dropdown,
		.header-mobile-social-media a,
		.primary-desktop .sh-nav > li.menu-item ul:not(.nav-tabs),
		.sh-header-mobile-dropdown {
			background-color: <?php echo esc_attr( $menu_background_color ); ?>!important;
		}
	<?php endif; ?>

 	<?php if( $menu_link_border_color ) : ?>
		.sh-nav-mobile li:after,
		.sh-nav-mobile ul:before {
			background-color: <?php echo esc_attr( $menu_link_border_color ); ?>!important;
		}
	<?php endif; ?>

 	<?php if( $menu_link_color ) : ?>
		.header-mobile-social-media a i,
		.sh-nav-mobile li a,
		.primary-desktop .sh-nav > li.menu-item ul a {
			color: <?php echo esc_attr( $menu_link_color ); ?>!important;
		}
	<?php endif; ?>

	.sh-nav-mobile .current_page_item > a,
	.sh-nav-mobile > li a:hover,
	.primary-desktop .sh-nav ul,
	.primary-desktop .sh-nav > li.menu-item ul li:hover > a,
	.primary-desktop .sh-nav > li.menu-item ul li:hover > a i,
	.primary-desktop .sh-nav ul.mega-menu-row li.mega-menu-col > a {
		color: <?php echo esc_attr( $menu_link_hover_color ); ?>!important;
	}

	.header-mobile-social-media,
	.header-mobile-social-media a {
		border-color: <?php echo esc_attr( $menu_link_border_color ); ?>!important;
	}

	/*.primary-desktop .sh-nav li.menu-item ul:not(.nav-tabs) {
		border: 1px solid <?php echo esc_attr( $menu_link_border_color ); ?>!important;
	}*/

	.sh-nav .mega-menu-row > li.menu-item {
		border-right: 1px solid <?php echo esc_attr( $menu_link_border_color ); ?>!important;
	}


<?php
/*-----------------------------------------------------------------------------------*/
/* Sidebar
/*-----------------------------------------------------------------------------------*/
?>

	#sidebar .widget-item .widget-title {
		<?php echo wp_kses_post( $sidebar_headings ); ?>
	}

	<?php if( $sidebar_border_color ) : ?>
		#sidebar .widget-item li {
			border-color: <?php echo esc_attr( $sidebar_border_color ); ?>!important;
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Footer
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $footer_width == 'full' ) : ?>
		@media (min-width: 1000px) {
			.sh-footer .container {
				width: 90%!important;
				max-width: 90%!important;
			}
		}
	<?php endif; ?>

	.sh-footer {
		<?php if( $footer_background_image ) : ?>
			background-image: url(<?php echo esc_url ($footer_background_image ); ?>);
		<?php endif; ?>
		background-size: cover;
		background-position: 50% 50%;
	}

	.sh-footer .sh-footer-widgets {
		background-color: <?php echo esc_attr( $footer_background_color ); ?>;
		color: <?php echo esc_attr( $footer_text_color ); ?>;
		<?php if( $footer_padding && $footer_padding != '100px 0px 100px 0px' ) : ?>
			padding: <?php echo esc_attr( $footer_padding ); ?>
		<?php endif; ?>
	}

	.sh-footer .sh-footer-widgets .post-meta,
	.sh-footer .sh-footer-widgets .sh-recent-posts-widgets-item-meta a {
		color: <?php echo esc_attr( $footer_text_color ); ?>;
	}

	.sh-footer .sh-footer-widgets i:not(.icon-link):not(.icon-magnifier),
	.sh-footer .sh-footer-widgets .widget_recent_entries li:before {
		color: <?php echo esc_attr( $footer_icon_color ); ?>!important;
	}

	.sh-footer .sh-footer-widgets h3 {
		<?php echo wp_kses_post( $footer_headings ); ?>
	}

	.sh-footer .sh-footer-widgets ul li,
	.sh-footer .sh-footer-widgets ul li,
	.widget_product_categories ul.product-categories a,
	.sh-recent-posts-widgets .sh-recent-posts-widgets-item,
	.sh-footer .sh-widget-posts-slider-style1:not(:last-child),
	.sh-footer-widgets .widget_tag_cloud a {
		border-color: <?php echo esc_attr( $footer_border_color ); ?>;
	}

	.sh-footer .post-meta-content > *:not(:last-child):not(:nth-last-child(2)):after,
	.sh-footer-widgets h3:not(.widget-tab-title):after,
	.sh-footer-widgets .sh-widget-poststab-title:after {
		background-color: <?php echo esc_attr( $footer_border_color ); ?>;
	}

	@media (max-width: 1025px) {
		.sh-footer .post-meta-content > *:nth-last-child(2):after {
			background-color: <?php echo esc_attr( $footer_border_color ); ?>;
		}
	}

	.sh-footer-widgets {
		border-bottom: 1px solid <?php echo esc_attr( $footer_bottom_border_color ); ?>;
	}

	.sh-footer .sh-footer-widgets a,
	.sh-footer .sh-footer-widgets .post-views,
	.sh-footer .sh-footer-widgets li a,
	.sh-footer .sh-footer-widgets h6,
	.sh-footer .sh-footer-widgets .sh-widget-posts-slider-style1 h5,
	.sh-footer .sh-footer-widgets .sh-widget-posts-slider-style1 h5 span,
	.sh-footer .widget_about_us .widget-quote {
		color: <?php echo esc_attr( $footer_link_color ); ?>;
	}

	.sh-footer .sh-footer-widgets a:hover,
	.sh-footer .sh-footer-widgets li a:hover,
	.sh-footer .sh-footer-widgets h6:hover {
		color: <?php echo esc_attr( $footer_hover_color ); ?>;
	}

	.sh-footer-columns > .widget-item {
		<?php if( $footer_columns == 1 ) : ?>
			width: 100%;
		<?php elseif( $footer_columns == 2 ) : ?>
			width: 50%;
		<?php elseif( $footer_columns == 4 ) : ?>
			width: 25%;
		<?php elseif( $footer_columns == 5 ) : ?>
			width: 20%;
		<?php endif; ?>
	}

	.sh-footer .sh-copyrights {
		background-color: <?php echo esc_attr( $copyright_background_color ); ?>;
		color: <?php echo esc_attr( $copyright_text_color ); ?>;
	}

	.sh-footer .sh-copyrights a,
	.sh-footer .sh-copyrights .sh-nav li.menu-item a {
		color: <?php echo esc_attr( $copyright_link_color ); ?>;
	}

	.sh-footer .sh-copyrights a:hover {
		color: <?php echo esc_attr( $copyright_hover_color ); ?>!important;
	}

	.sh-footer .sh-copyrights-social a {
		border-left: 1px solid <?php echo esc_attr( $copyright_border_color ); ?>;
	}

	.sh-footer .sh-copyrights-social a:last-child {
		border-right: 1px solid <?php echo esc_attr( $copyright_border_color ); ?>;
	}

	@media (max-width: 850px) {
		.sh-footer .sh-copyrights-social a {
			border: 1px solid <?php echo esc_attr( $copyright_border_color ); ?>;
		}
	}


<?php
/*-----------------------------------------------------------------------------------*/
/* Blog / Posts
/*-----------------------------------------------------------------------------------*/
if( $post_meta == 'enabled_single' || $post_meta == 'disabled' ) : ?>
	.post-meta {
		display: none!important;
	}

	<?php if( $post_meta == 'enabled_single' ) :?>
		.post-single-meta .post-meta {
			display: block!important;
		}
	<?php endif; ?>
<?php endif;


/*-----------------------------------------------------------------------------------*/
/* WooCommerce
/*-----------------------------------------------------------------------------------*/
?>

	.gillion-woocommerce #content:not(.page-content) ul.products li.product {
		<?php if( $wc_columns == 3 ) : ?>
			width: 33.3%;
		<?php elseif( $wc_columns == 2 ) : ?>
			width: 50%;
		<?php else : ?>;
			width: 25%;
		<?php endif; ?>
	}


<?php
/*-----------------------------------------------------------------------------------*/
/* Page Layout
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $page_layout == 'boxed' ) :
		$background_image = gillion_get_image( $page_layout_atts['page_background_image'] );
		$current = 0;
		if( $page_layout_atts['specific_pages'] ) :
			$limits = explode( ",", $page_layout_atts['specific_pages'] );
			foreach( $limits as $limit ) :
				if( intval( $limit ) == gillion_page_id() ) :
					$current = 1;
				endif;
			endforeach;
		else :
			$limits = '';
		endif;

		if( is_string( $limits ) || ( count( $limits ) > 0 && $current == 1 ) ) : ?>
			<?php if( $page_layout_atts['page_background_color'] ) : ?>
				body {
					background-color: <?php echo esc_attr( $page_layout_atts['page_background_color'] ); ?>!important;
					<?php if( $background_image ) : ?>
						background-image: url(<?php echo esc_url( $background_image ); ?>);
						background-size: cover;
						background-position: center bottom;
						background-repeat: no-repeat;
					<?php endif; ?>
				}
			<?php endif; ?>

			body #page-container {
				position: relative;
				max-width: 1200px!important;
				margin: 0 auto;

				<?php if( $page_layout_atts['content_background_color'] ) : ?>
					background-color: <?php echo esc_attr( $page_layout_atts['content_background_color'] ); ?>!important;
				<?php endif; ?>

				<?php if( $page_layout_atts['page_radius'] ) : ?>
					border-top-left-radius: <?php echo esc_attr( $page_layout_atts['page_radius'] ); ?>;
					border-top-right-radius: <?php echo esc_attr( $page_layout_atts['page_radius'] ); ?>;
					overflow: hidden;
				<?php endif; ?>

				<?php if( $page_layout_atts['margin_top'] ) : ?>
					margin-top: <?php echo esc_attr( $page_layout_atts['margin_top'] ); ?>;
					padding-top: 0px!important;
				<?php endif; ?>

				<?php if( $page_layout_atts['border_style'] == 'shadow' ) : ?>
					box-shadow: 0px 6px 30px rgba(0,0,0,0.1);
				<?php elseif( $page_layout_atts['border_style'] == 'line' ) : ?>
					border-left: 1px solid rgba(0,0,0,0.07);
					border-right: 1px solid rgba(0,0,0,0.07);
					border-bottom: 1px solid rgba(0,0,0,0.07);
				<?php endif; ?>
			}

			#page-container .container {
				width: 100%!important;
				min-width: 100%!important;
				max-width: 100%!important;
				padding-left: 45px!important;
				padding-right: 45px!important;
			}

			.sh-sticky-header-active {
				max-width: 1200px!important;
				margin: 0 auto;
			}
		<?php endif; ?>
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Titlebar
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( gillion_option_image( 'titlebar-background' ) ) : ?>
		.sh-titlebar {
			background-image: url(<?php echo esc_url( gillion_option_image( 'titlebar-background' ) ); ?>);
		}
	<?php endif; ?>

	<?php if( gillion_option( 'titlebar-background-color' ) ) : ?>
		.sh-titlebar {
			background-color: <?php echo esc_attr( gillion_option( 'titlebar-background-color') ); ?>;
		}
	<?php endif; ?>

	<?php if( gillion_option( 'titlebar-title-color' ) ) : ?>
		.sh-titlebar .titlebar-title h1 {
			color: <?php echo esc_attr( gillion_option( 'titlebar-title-color') ); ?>;
		}
	<?php endif; ?>

	<?php if( gillion_option( 'titlebar-breadcrumbs-color' ) ) : ?>
		.sh-titlebar .title-level a,
		.sh-titlebar .title-level span {
			color: <?php echo esc_attr( gillion_option( 'titlebar-breadcrumbs-color') ); ?>!important;
		}
	<?php endif; ?>

	<?php if( $header_bottom_border == 'off' && $page == 1 ) : ?>
		.sh-header:not(.sh-sticky-header-active),
		.sh-header:not(.sh-sticky-header-active) > .sh-header-standard {
			border-width: 0px!important;
			border-bottom-width: 0px!important;
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Crispy Images
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $crispy_images == true ) : ?>
		img {
			-webkit-backface-visibility: hidden;
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Mega menu
/*-----------------------------------------------------------------------------------*/
?>

<?php
	$locations = get_nav_menu_locations();
	if( isset( $locations[ 'header' ] ) ) :
		$menu = wp_get_nav_menu_object( $locations[ 'header' ] );
		$menuitems = wp_get_nav_menu_items( $menu->term_id );
		foreach( $menuitems as $menuitem ) :
			$id = $menuitem->ID;
			$item_type = fw_ext_mega_menu_get_db_item_option( $id, 'type' );
			if( $item_type == 'row' ) :
				$mega_bs = fw_ext_mega_menu_get_db_item_option( $id, 'row/background_image' );
				if( gillion_get_image_size( $mega_bs ) ) : ?>

					.menu-item-273 .mega-menu-row {
						background-image: url( <?php echo esc_url( gillion_get_image_size( $mega_bs ) ); ?> )!important;
					}

				<?php endif;
			endif;
		endforeach;
	endif;
?>

<?php
/*-----------------------------------------------------------------------------------*/
/* RTL Support
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $rtl_support == true ) : ?>
		body {
			text-align: right;
		}

		.sh-comments-required-notice {
			float: left;
		}

		.widget_search .search-field {
			text-align: right;
		}

		.widget_search .search-submit {
			right: auto;
			left: 5px;
		}

		.sh-recent-posts-widgets-item-thumb {
			right: 0;
			left: auto;
		}

		.sh-recent-posts-widgets-item-content {
			padding-left: 0px;
			padding-right: 80px;
		}

		.post-meta-comments > a {
			float: left!important;
		}

		.sh-blog-social div {
			max-width: 600px;
		}

		.sh-contacts-widget-item {
			padding-left: 0px;
			padding-right: 40px!important;
		}

		.sh-contacts-widget-item i {
			right: 0!important;
			left: auto;
		}

		.titlebar-title-h1 {
			text-align: right;
		}

		.sh-nav li.menu-item li.menu-item-has-children > a:after {
			float: left;
		}

		.sh-list-item:after,
		.post-content:before {
			content: "";
			display: block;
			clear: both;
		}

		.post-item h2,
		.post-item .post-content {
			text-align: right;
		}

		input,
		textarea {
			text-align: right;
		}

		.sh-nav {
			float: left;
		}

		.sh-header .sh-nav li.menu-item:first-child {
			padding-left: 0px!important;
		}

		.sh-header .header-logo {
			float: right;
		}

		.sh-copyrights-logo {
			padding-right: 0px;
			padding-left: 25px;
		}

		.menu-item.sh-nav-search {
			padding: 0!important;
		}

		.header-social-media {
			text-align: left;
		}

	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Back to top button - rounded
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $back_to_top_rounded && $back_to_top_rounded != '0px' ) : ?>
		.sh-back-to-top {
			border-radius: <?php echo esc_attr( $back_to_top_rounded ); ?>
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* 404 Page
/*-----------------------------------------------------------------------------------*/
?>

	.sh-404-left .sh-ratio-content {
		background-image: url(<?php echo esc_url( gillion_option_image('404_image') ); ?>);
	}

	.sh-404-page .sh-404-overay {
		<?php
			if( $page_404_background && $page_404_background2 ) :
				echo 'background-color: '.esc_attr( $page_404_background ).'; background: linear-gradient(to bottom, '.esc_attr( $page_404_background ).' 0%,'.esc_attr( $page_404_background2 ).' 100%);';
			elseif( $page_404_background ) :
				echo 'background-color: '.esc_attr( $page_404_background ).';';
			endif;
		?>
	}


<?php
/*-----------------------------------------------------------------------------------*/
/* Titlebar
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $titlebar_background ) : ?>
		.sh-titlebar {
			background-image: url( <?php echo esc_url( $titlebar_background ); ?> );
		}
	<?php endif; ?>


<?php
/*-----------------------------------------------------------------------------------*/
/* Page Loader
/*-----------------------------------------------------------------------------------*/
$page_loader = 0;
if( gillion_option('page_loader', 'off') != 'off' ) :
	if( gillion_option('page_loader') == 'on2' ) :

		if (strpos(wp_get_referer(), esc_url( home_url('/') ) ) !== false) :
			$page_loader = 0;
		else :
			$page_loader = 1;
		endif;

	else :

		$page_loader = 1;

	endif;
endif;
?>

	<?php if( $page_loader == 1 ) : ?>
		body {
			overflow: hidden;
		}

		.sh-page-loader {
			background-color: <?php echo esc_attr( gillion_option('page_loader_background_color') ); ?>;
		}

		.sk-cube-grid .sk-cube,
		.sk-folding-cube .sk-cube:before,
		.sk-spinner > div,
		.sh-page-loader-style-spinner .object {
			background-color: <?php echo ( gillion_option('page_loader_accent_color') ) ? esc_attr(gillion_option('page_loader_accent_color')) : esc_attr(gillion_option('accent_color')); ?>!important;
		}
	<?php endif; ?>

<?php
/*-----------------------------------------------------------------------------------*/
/* Page - White Borders
/*-----------------------------------------------------------------------------------*/
?>

	<?php if( $white_borders == true ) : ?>
		body.admin-bar.page-white-borders .sh-window-line.line-top,
		body.admin-bar.page-white-borders .sh-window-line.line-left,
		body.admin-bar.page-white-borders .sh-window-line.line-right {
			top: 32px;
		}

		body.page-white-borders #page-container {
			padding-top: 20px;
		}

		body.admin-bar.page-white-borders #page-container {
			padding-top: 52px!important;
		}

		body.page-white-borders.page-layout-right-fixed .sh-window-line.line-top {
			top: 0!important;
		}

		body.page-white-borders .sh-sticky-header-active {
			top: 20px!important;
			left: 20px!important;
			right: 20px!important;
			width: auto!important;
		}

		body.admin-bar.page-white-borders .sh-sticky-header-active {
			top: 52px!important;
		}
	<?php endif; ?>

<?php else : ?>

	.post-meta,
	.post-categories,
	.post-switch-item-right,
	.sh-read-later-review-score,
	.sh-header .sh-nav li.menu-item a,
	.sh-comment-date a,
	.post-button .post-button-text,
	.widget_categories li,
	.sh-dropcaps,
	.sh-dropcaps-full-square,
	.sh-dropcaps-full-square-border,
	.sh-dropcaps-full-square-tale,
	.sh-dropcaps-square-border,
	.sh-dropcaps-square-border2,
	.sh-dropcaps-circle,
	.comment-body .reply,
	.sh-comment-form label,
	blockquote,
	blockquote:after,
	.sh-heading-font,
	.post-review-score,
	.sh-comment-author a,
	.sh-header-top .sh-nav li.menu-item a {
		font-family: Montserrat;
	}

	.post-title h2:hover {
		color: #d79c74;
	}

	.primary-desktop .sh-header-top {
		background-color: #313131;
	}

	.blog-single .post-title h2:hover,
	.post-password-form label,
	.sh-page-links p {
		color: #3f3f3f!important;
	}

	.sh-default-color a,
	.sh-default-color,
	#sidebar a,
	.logged-in-as a ,
	.sh-social-share-networks .jssocials-share i {
		color: #8d8d8d!important;
	}

	.sh-sidebar-search-active .search-field,
	.post-password-form input[type="submit"] {
		background-color: #d79c74;
	}

	.post-password-form input[type="submit"]:hover {
		background-color: #21bee2;
	}

	.sh-sidebar-search-active .search-field,
	.sh-sidebar-search-active .search-submit i {
		color: #fff;
	}

	.sh-sidebar-search-active .search-field {
		border-color: #d79c74!important;
	}

	.sh-back-to-top {
		border-radius: 100px;
	}

	/* Elements CSS */

<?php endif;
ob_end_flush();
?>
