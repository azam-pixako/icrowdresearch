<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); } ?>

<?php echo wp_kses_post( $before_widget ); ?>

<?php
	if( $atts['title'] ) :
		echo '<h3 class="widget-title">'.esc_attr( $atts['title'] ).'</h3>';
	endif;
?>

<?php

function gillion_instagram_followers( $client_id = '', $access_token = '' ) {
	$cache_name = esc_attr( 'instagram_x_'.$client_id.'_'.$access_token );
	$client_id = (( !$client_id && $access_token )) ? (int)$access_token : $client_id;

	if( $client_id && $access_token ) :
		if ( false === ( $instagram_follows = get_transient( esc_attr( $cache_name ) ) ) ) {
			$instagram = "https://api.instagram.com/v1/users/$client_id/?access_token=$access_token";
			$instagram_follows = json_decode( wp_remote_retrieve_body( wp_remote_get( $instagram ) ) )->data->counts->followed_by;
			set_transient( esc_attr( $cache_name ), $instagram_follows, 12 * 'HOURS_IN_SECONDS' );
		}

		if( count($instagram_follows) ) :
			return $instagram_follows;
		else :
			$followers = false;
		endif;
	endif;
}

function gillion_facebook_followers( $fbid = '', $app_id = '', $app_secret = '' ) {
	$cache_name = esc_attr( 'facebook_x_'.$fbid.'_'.$app_id.'_'.$app_secret );
	if( $fbid && $app_id && $app_secret ) :

		if ( false === ( $followers = get_transient( esc_attr( $cache_name ) ) ) ) {
			$url = 'https://graph.facebook.com/v2.7/'. $fbid . '?fields=fan_count&access_token='. $app_id . '|' . $app_secret;
			$followers = json_decode( wp_remote_retrieve_body( wp_remote_get( $url ) ) )->fan_count;
			set_transient( esc_attr( $cache_name ), $followers, 12 * 'HOURS_IN_SECONDS' );
		}

		if( is_numeric( $followers ) ) :
			return $followers;
		else :
			$followers = false;
		endif;
	endif;
}

function gillion_youtube_followers( $youtube_channel_id = '', $google_api_key = '' ) {
	$cache_name = esc_attr( 'youtube_x_'.$youtube_channel_id.'_'.$google_api_key );
	if( $youtube_channel_id && $google_api_key ) :

		if ( false === ( $followers = get_transient( esc_attr( $cache_name ) ) ) ) {
			$url = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=".$youtube_channel_id."&key=".$google_api_key;
			$followers = json_decode( wp_remote_retrieve_body( wp_remote_get( $url ) ) );

			if( isset( $followers->items[0]) ) :
				$followers = intval( $followers->items[0]->statistics->subscriberCount );
			else :
				$followers = false;
			endif;

			set_transient( esc_attr( $cache_name ), $followers, 12 * 'HOURS_IN_SECONDS' );
		}

		if( is_numeric( $followers ) ) :
			return $followers;
		endif;
	endif;
}

function gillion_googleplus_followers( $googleplus_id = '', $google_api_key = '' ) {
	$cache_name = esc_attr( 'gplus_xz_'.$googleplus_id.'_'.$google_api_key );
	if( $googleplus_id && $google_api_key ) :

		if ( false === ( $followers = get_transient( esc_attr( $cache_name ) ) ) ) {
			$url = "https://www.googleapis.com/plus/v1/people/".$googleplus_id."?key=".$google_api_key;
			$followers = json_decode( wp_remote_retrieve_body( wp_remote_get( $url ) ) );

			if( isset( $followers->circledByCount ) ) :
				$followers = intval( $followers->circledByCount );
			else :
				$followers = false;
			endif;

			var_dump( $followers );

			set_transient( esc_attr( $cache_name ), $followers, 12 * 'HOURS_IN_SECONDS' );
		}

		if( is_numeric( $followers ) ) :
			return $followers;
		endif;
	endif;
}
?>

<?php
if( $atts['demo_mode'] != true ) :
	$facebook = ( $atts['facebook_username'] && $atts['facebook_app_id'] && $atts['facebook_app_secret'] ) ? gillion_facebook_followers( $atts['facebook_username'], $atts['facebook_app_id'], $atts['facebook_app_secret'] ) : false;
	$youtube = ( $atts['youtube_channel_id'] && $atts['youtube_api_key'] ) ? gillion_youtube_followers( $atts['youtube_channel_id'], $atts['youtube_api_key'] ) : false;
	$googleplus = ( $atts['googleplus_id'] && $atts['googleplus_api_key'] ) ? gillion_googleplus_followers( $atts['googleplus_id'], $atts['googleplus_api_key'] ) : false;
	$instagram = ( $atts['instagram_access_token'] ) ? gillion_instagram_followers( $atts['instagram_client_id'], $atts['instagram_access_token'] ) : false;
else :
	$facebook = 1243;
	$youtube = 256;
	$googleplus = 176;
	$instagram = 365;
endif;
?>

<div class="sh-widget-connected-list">
	<?php if( is_numeric( $facebook ) ) : ?>
		<a href="https://www.facebook.com/<?php echo esc_attr( $atts['facebook_username'] ); ?>/" target="_blank" class="sh-widget-connected-item sh-widget-connected-facebook sh-columns">
			<div class="sh-widget-connected-title"><i class="fa fa-facebook"></i> <?php esc_html_e( 'Like', 'gillion' ); ?></div>
			<div class="sh-widget-connected-count"><span><?php echo esc_attr( $facebook ); ?></span></div>
		</a>
	<?php endif; ?>

	<?php if( is_numeric( $googleplus) ) : ?>
		<a href="<?php echo ( $atts['googleplus_id'] ) ? 'https://plus.google.com/u/0/'.esc_attr( $atts['googleplus_id'] ).'/' : 'https://plus.google.com'; ?>" target="_blank" class="sh-widget-connected-item sh-widget-connected-gplus sh-columns">
			<div class="sh-widget-connected-title"><i class="fa fa-google-plus"></i> <?php esc_html_e( 'Subscribe', 'gillion' ); ?></div>
			<div class="sh-widget-connected-count"><span><?php echo esc_attr( $googleplus ); ?></span></div>
		</a>
	<?php endif; ?>

	<?php if( is_numeric( $instagram ) ) : ?>
		<a href="<?php echo ( $atts['instagram_username'] ) ? 'https://www.instagram.com/'.esc_attr( $atts['instagram_username'] ).'/' : 'https://www.instagram.com'; ?>" target="_blank" class="sh-widget-connected-item sh-widget-connected-instagram sh-columns">
			<div class="sh-widget-connected-title"><i class="fa fa-instagram"></i> <?php esc_html_e( 'Follow', 'gillion' ); ?></div>
			<div class="sh-widget-connected-count"><span><?php echo esc_attr( $instagram ); ?></span></div>
		</a>
	<?php endif; ?>

	<?php if( is_numeric( $youtube ) ) : ?>
		<a href="<?php echo ( $atts['youtube_channel_id'] ) ? 'https://www.youtube.com/channel/'.esc_attr( $atts['youtube_channel_id'] ).'/' : 'https://www.youtube.com'; ?>" target="_blank" class="sh-widget-connected-item sh-widget-connected-youtube sh-columns">
			<div class="sh-widget-connected-title"><i class="fa fa-youtube"></i> <?php esc_html_e( 'Subscribe', 'gillion' ); ?></div>
			<div class="sh-widget-connected-count"><span><?php echo esc_attr( $youtube ); ?></span></div>
		</a>
	<?php endif; ?>
</div>

<?php echo wp_kses_post( $after_widget ); ?>
