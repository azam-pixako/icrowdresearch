<?php
/**
 * Footer
 */
$page_layout_val = gillion_option('page_layout');
$page_layout = ( isset( $page_layout_val['page_layout'] ) ) ? esc_attr($page_layout_val['page_layout']) : 'line';
$page_layout_atts = gillion_get_picker( $page_layout_val );
wp_reset_postdata();
?>

		<?php if( gillion_post_option( get_the_ID(), 'page_layout' ) != 'full' ) : ?>
			</div>
		<?php endif; ?>
		</div>

		<?php
			/* Include footer instagram feed */
			get_template_part('inc/templates/footer-instagram' );
		?>

	<?php if( $page_layout == 'boxed' && $page_layout_atts['footer_width'] == 'full' ) : ?>
		</div></div>
	<?php endif; ?>

			<footer class="sh-footer">
				<?php
					if( gillion_footer_enabled() == 'on' ) :
						/* Inlcude theme footer widgets */
						get_template_part('inc/templates/footer-widgets' );
					endif;

					if( gillion_copyrights_enabled() == 'on' ) :
						/* Inlcude theme footer copyrights */
						get_template_part('inc/templates/footer-copyrights' );
					endif;
				?>
			</footer>

		<?php if( $page_layout != 'boxed' || $page_layout_atts['footer_width'] != 'full' ) : ?>
			</div>
		<?php endif; ?>


		<?php if( gillion_post_option( get_the_ID(), 'back_to_top' ) != 'none' ) :

			/* Inlcude back to top button HTML */
			get_template_part('inc/templates/back_to_top' );

			/* Inlcude login popup */
			get_template_part('inc/templates/login_popup' );
		endif; ?>
	<?php if( $page_layout != 'boxed' || $page_layout_atts['footer_width'] != 'full' ) : ?>
		</div>
	<?php endif; ?>

	<?php wp_reset_postdata(); wp_footer(); ?>
</body>
</html>
