<?php
/**
 * Single Product Sale Flash
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/sale-flash.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

if ( $product->is_in_stock() && $product->is_on_sale() ) {
	echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale', 'gillion' ) . '</span>', $post, $product );
} else if( ! $product->is_in_stock() ) {
	echo apply_filters( 'woocommerce_sale_flash', '<span class="outofstock">' . esc_html__( 'Out of stock', 'gillion' ) . '</span>', $post, $product );
} else if( gillion_option( 'wc_new', true ) == true && ( time() - ( 60 * 60 * 24 * 3 ) ) < get_the_time( 'U' ) ) {
	echo apply_filters( 'woocommerce_sale_flash', '<span class="newproduct">' . esc_html__( 'New', 'gillion' ) . '</span>', $post, $product );
}

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
