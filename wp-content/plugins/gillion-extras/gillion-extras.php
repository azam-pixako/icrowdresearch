<?php
/**
* Plugin Name: Gillion Extras
* Plugin URI: http://gillion.shufflehound.com
* Description: Adds extra functionality to Gillion theme like social icons for authors
* Version: 1.0.0
* Author: Shufflehound
* Author URI: http://shufflehound.com
* License: GNU General Public License v2 or later
*/


/**
 * Add Social Media for Author (plugin territory)
 */
if ( ! function_exists( 'gillion_user_socialmedia' ) ) :
    function gillion_user_socialmedia( $social ) {
        $social['facebook'] = 'Facebook';
        $social['twitter'] = 'Twitter';
        $social['google-plus'] = 'Google Plus';
        $social['instagram'] = 'Instagram';
        $social['linkedin'] = 'LinkedIn';
        $social['pinterest'] = 'Pinterest';
        $social['tumblr'] = 'Tumblr';
        $social['youtube'] = 'Youtube';
        return $social;
    }
    add_filter('user_contactmethods','gillion_user_socialmedia',10,1);
endif;